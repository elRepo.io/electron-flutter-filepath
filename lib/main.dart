
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hello_world/javascript_controller.dart'; //import dart:js library

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {

  final String? title;

  const MyHomePage({Key? key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String selectedFile = 'Hello, World!';
  String? base64image;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          // js.context.callMethod("showSelectFileDialog", []); // Can't be awaited
          // var result = await promiseToFuture(getFilePath());
          // https://stackoverflow.com/questions/62692105/how-to-asynchronously-call-javascript-in-flutter
          // https://www.fluttercampus.com/guide/54/how-to-run-javascript-on-button-click-in-flutter-web/
          // https://dev.to/graphicbeacon/how-to-use-javascript-libraries-in-your-dart-applications--4mc6
          selectedFile = await getFutureFilePath();
          base64image = await getBase64FromPath(selectedFile);
          setState(() {

          });
        },
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if(base64image != null) Image.memory(base64Decode(base64image!)),
            Text(
              selectedFile,
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
