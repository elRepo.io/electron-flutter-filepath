@JS()
library main;

import 'package:js/js.dart';
import 'dart:js_util';

@JS('api.getFileAction')
external getFileAction();

Future<String> getFutureFilePath() async =>
    (await promiseToFuture(getFileAction()))[0];

@JS('api.getBase64image')
external getBase64image(String path);

Future<String?> getBase64FromPath(String path) async =>
    await promiseToFuture(getBase64image(path));
