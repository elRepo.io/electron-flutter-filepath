const { ipcRenderer, contextBridge } = require('electron')

// Adds an object 'api' to the global window object:
contextBridge.exposeInMainWorld('api', {
    getFileAction: async (arg) => {
        return await ipcRenderer.invoke('select-dirs', arg);
    },
    getBase64image: async (arg) => {
            return await ipcRenderer.invoke('read-base64image', arg);
        }
});


