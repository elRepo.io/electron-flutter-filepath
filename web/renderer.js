// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
// https://stackoverflow.com/questions/45148110/how-to-add-a-callback-to-ipc-renderer-send

//async function showSelectFileDialog () {
//    const response = await window.api.dirsAction();
//    console.log(response); // we now have the response from the main thread without exposing
//                           // ipcRenderer, leaving the app less vulnerable to attack
//    return response;
//}
//
//(async () => {
//    const response = await window.api.doAction();
//    console.log(response); // we now have the response from the main thread without exposing
//                           // ipcRenderer, leaving the app less vulnerable to attack
//})();

//function showSelectFileDialog() {
//    window.postMessage({
//      type: 'select-dirs',
//    })
//}

//document.getElementById('dirs').addEventListener('click', (evt) => {
//    evt.preventDefault()
//    window.postMessage({
//      type: 'select-dirs',
//    })
//  })