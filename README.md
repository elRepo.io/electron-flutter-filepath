# electron-flutter-filepath

Demonstration of how to get a file system absolute file path from an electron wrapped web flutter app

## To Use

```bash
# Clone this repository
# Go into the repository
#install flutter dependencies
flutter pub get
# Install dependencies
npm install
# Run the app
npm run dev
```

Maybe you need to `cntl` + `shift` + `R` to reload the electron window after flutter server is started


## Release
To build for production, first run `npm run build`.

The output directory, `build` contains a `package.json` pointing to the built
application, and can be run as follows:

```bash
cd build
npm install
npm start
```
## Package
The output directory's package manifest, build/package.json, includes electron-packager, and therefore can be packaged for distribution on Windows, MacOS, and Linux.

to build for the host platform run:
``` 
npm run build
```
To build for all platforms, run :

``` 
npm run package
```
